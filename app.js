const yargs = require('yargs');
const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

var args = yargs.options({
    a: {
        demand: true,
        alias: 'address',
        descripe: 'Address of fetch weather for.',
        string:true
    }
})
.help()
.argv;

geocode.geocodeAddress(args.address, (errorMsg, results) => {
    if(errorMsg) {
        console.log(errorMsg);
    } else {
        var latitude = results.latitude;
        var longitude = results.longitude;

        weather.getWeather(latitude, longitude, (errorMsg, weatherResults) => {
            if(errorMsg) {
                console.log(errorMsg);
            } else {
                console.log(results.address);
                console.log(`It's ${weatherResults.summary} today, Currently ${weatherResults.temperature}, It feels like ${weatherResults.apparentTemperature}`);
            }
        });
    }
});
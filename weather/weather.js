const request = require('request');
const api_key = 'f2b9d5268e15f1de25e0038cce785350';

var getWeather = (latitude, langitude, callback) => {
    request({
        url: `https://api.darksky.net/forecast/${api_key}/${latitude},${langitude}`,
        json: true
    }, (error, response, data) => {
        if(error && response.code === 400) {
            callback('Unable to find weather..');
        } else {
            callback(undefined, {
                temperature: data.currently.temperature,
                apparentTemperature: data.currently.apparentTemperature,
                summary: data.currently.summary
            });
        }
    });
}

module.exports = {
    getWeather
};
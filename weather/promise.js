const axios = require('axios');
const yargs = require('yargs');

const api_key = 'f2b9d5268e15f1de25e0038cce785350';

var args = yargs.options({
    a: {
        demand: true,
        alias: 'address',
        descripe: 'Address of fetch weather for.',
        string:true
    }
})
.help()
.argv;

var encodeAddress = encodeURIComponent(args.address);
var encodeUrl = `http://maps.googleapis.com/maps/api/geocode/json?address=${encodeAddress}`;

axios.get(encodeUrl).then((response) => {
    if(response.data.status === 'ZERO_RESULTS') {
        throw new Error('Unable to fine this address..');
    }

    var latitude = response.data.results[0].geometry.location.lat;
    var langitude = response.data.results[0].geometry.location.lng;
    var weatherUrl = `https://api.darksky.net/forecast/${api_key}/${latitude},${langitude}`;
    console.log(response.data.results[0].formatted_address);
    return axios.get(weatherUrl);
})
.then((response) => {
    if(response.code === 400) {
        throw new Error('Unable to find weather..');
    }
    console.log(`It's ${response.data.currently.summary} today, Currently ${response.data.currently.temperature}, It feels like ${response.data.currently.apparentTemperature}`);
})
.catch((e) => {
    if(e.code === 'ENOTFOUND') {
        console.log('Unable to connect to api server..');
    } else {
        console.log(e.message);
    }
});
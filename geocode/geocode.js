const request = require('request');

var geocodeAddress = (address, callback) => {
    var encodeAddress = encodeURIComponent(address);
    request({
        url: `http://maps.googleapis.com/maps/api/geocode/json?address=${encodeAddress}`,
        json: true
    }, (error, response, data) => {
        if(error) {
            callback('Unable to connect for Google server..');
        } else if(data.status === 'ZERO_RESULTS') {
            callback('Unable to find this address..');
        } else if(data.status === 'OK') {
            callback(undefined, {
                address: data.results[0].formatted_address,
                latitude:data.results[0].geometry.location.lat,
                longitude: data.results[0].geometry.location.lng
            });
        }
    });
}

module.exports = {
    geocodeAddress
};
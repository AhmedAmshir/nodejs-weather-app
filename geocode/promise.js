const request = require('request');

var geocode = (address) => {
    return new Promise((resolve, reject) => {
        var encodeAddress = encodeURIComponent(address);
        var encodeUrl = `http://maps.googleapis.com/maps/api/geocode/json?address=${encodeAddress}`;
        request({
            url: encodeUrl,
            json: true
        }, (error, response, data) => {
            if(error) {
                reject('Unable to connect for Google server..');
            } else if(data.status === 'ZERO_RESULTS') {
                reject('Unable to find this address..');
            } else if(data.status === 'OK') {
                resolve({
                    address: data.results[0].formatted_address,
                    latitude:data.results[0].geometry.location.lat,
                    longitude: data.results[0].geometry.location.lng
                });
            }
        });
    });
}

geocode('egypt').then((res) => {
    console.log(JSON.stringify(res, undefined, 2));
},(errorMsg) => {
    console.log(errorMsg);
});